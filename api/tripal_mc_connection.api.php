<?php

/**
 * @file
 * Provides an application programming interface (API) for Chado connection.
 */

/**
 * @defgroup tripal_mc_connection_api Multi-Chado Chado Connection API
 * @ingroup tripal_mc
 * @{
 * Provides an application programming interface (API) for working with Chado
 * Connection entitites
 * @}
 */

/**
 * Load a Chado connection.
 */
function tripal_mc_connection_load($mccid, $reset = FALSE) {
  $chado_connections = tripal_mc_connection_load_multiple(array($mccid), array(), $reset);
  return reset($chado_connections);
}

/**
 * Load multiple Chado connections based on certain conditions.
 */
function tripal_mc_connection_load_multiple($mccid = array(), $conditions = array(), $reset = FALSE) {
  return entity_load(TRIPAL_MC_CONNECTION_ENTITY_TYPE, $mccid, $conditions, $reset);
}

/**
 * Save Chado connection.
 */
function tripal_mc_connection_save($chado_connection) {
  entity_save(TRIPAL_MC_CONNECTION_ENTITY_TYPE, $chado_connection);
}

/**
 * Delete single Chado connection.
 */
function tripal_mc_connection_delete($chado_connection) {
  entity_delete(TRIPAL_MC_CONNECTION_ENTITY_TYPE, entity_id(TRIPAL_MC_CONNECTION_ENTITY_TYPE, $chado_connection));
}

/**
 * Delete multiple Chado connections.
 */
function tripal_mc_connection_delete_multiple($mccids) {
  entity_delete_multiple(TRIPAL_MC_CONNECTION_ENTITY_TYPE, $mccids);
}

/**
 * Encrypt a given clear password using Multi-Chado salt.
 */
function tripal_mc_encrypt_password($password) {
  // Get current salt.
  $password_salt = variable_get(TRIPAL_MC_SALT_VAR);
  if (!$password_salt) {
    drupal_set_message(t('Failed to get Multi-Chado password salt! You may have to uninstall and reinstall this module.'), 'error');
    return '';
  }

  // Get password length.
  $password_length = strlen($password);
  // Make sure salt length is enough.
  $full_password_salt = $password_salt;
  while (strlen($full_password_salt) < $password_length) {
    $full_password_salt .= $password_salt;
  }

  // Encrypt.
  $crypted_password = '';
  for ($i = 0; $i < $password_length; ++$i) {
    $crypted_byte = chr(ord($password[$i]) ^ ord($full_password_salt[$i]));
    $crypted_password .= $crypted_byte;
  }

  return base64_encode($crypted_password);
}

/**
 * Decrypt a given crypted password using Multi-Chado salt.
 */
function tripal_mc_decrypt_password($crypted_password) {
  // Get current salt.
  $password_salt = variable_get(TRIPAL_MC_SALT_VAR);
  if (!$password_salt) {
    drupal_set_message(t('Failed to get Multi-Chado password salt! You may have to uninstall and reinstall this module.'), 'error');
    return '';
  }

  $crypted_password = base64_decode($crypted_password);

  // Get password length.
  $password_length = strlen($crypted_password);
  // Make sure salt length is enough.
  $full_password_salt = $password_salt;
  while (strlen($full_password_salt) < $password_length) {
    $full_password_salt .= $password_salt;
  }

  // Decrypt.
  $password = '';
  for ($i = 0; $i < $password_length; ++$i) {
    $clear_byte = chr(ord($crypted_password[$i]) ^ ord($full_password_salt[$i]));
    $password .= $clear_byte;
  }

  return $password;
}
