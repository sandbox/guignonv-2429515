<?php

/**
 * @file
 * Defines Multi-Chado global constants.
 */

define('TRIPAL_MC_CONNECTION_TABLE', 'tripal_mc_connection');
define('TRIPAL_MC_CONNECTION_COLUMN', 'tripal_mc_mccid');
define('TRIPAL_MC_SYNC_TABLE_PREFIX', 'tripal_mc_');
define('TRIPAL_MC_SYNC_TABLES_VAR', 'tripal_mc_sync_tables');
define('TRIPAL_MC_SALT_VAR', 'tripal_mc_salt');
define('TRIPAL_MC_CONNECTION_ENTITY_TYPE', 'tripal_mc_connection');
define('TRIPAL_MC_ASSOCIATED_USER_FIELD', 'field_tripal_mc_au');
define('TRIPAL_MC_CONNECTION_BASE_ALIAS', 'tripal_mc/connection');
