<?php

/**
 * @file
 * TripalMCConnectionController class definition.
 */

/**
 * Entity controller for Chado connection entities (TripalMCConnection).
 */
class TripalMCConnectionController extends EntityAPIControllerExportable {

  /**
   * Creates a new Chado connection.
   */
  public function create(array $values = array()) {
    global $user;
    $current_connection_info = Database::getConnectionInfo();
    $values += array(
      'mccid' => '',
      'title' => 'Chado Connection',
      'machine_name' => '',
      'host' => $current_connection_info['default']['host'],
      'port' => $current_connection_info['default']['port'],
      'username' => '',
      'password' => '',
      'database' => $current_connection_info['default']['database'],
      'schemaname' => 'chado',
      'description' => '',
      'uid' => $user->uid,
      'type' => TRIPAL_MC_CONNECTION_ENTITY_TYPE,
    );
    return parent::create($values);
  }

  /**
   * Fills Chado connection field settings.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $db_url = 'pgsql://' . $entity->username . '@' . $entity->host . ':' . $entity->port . '/' . $entity->database;
    $content['database'] = array(
      '#theme'               => 'field',
      '#weight'              => -2,
      '#title'               => t('Database'),
      '#access'              => TRUE,
      '#label_display'       => 'inline',
      '#view_mode'           => 'full',
      '#language'            => LANGUAGE_NONE,
      '#field_translatable' => FALSE,
      '#field_name'         => 'database',
      '#field_type'         => 'text',
      '#entity_type'        => TRIPAL_MC_CONNECTION_ENTITY_TYPE,
      '#bundle'             => TRIPAL_MC_CONNECTION_ENTITY_TYPE,
      '#items'              => array(array('value' => $db_url)),
      '#formatter'          => 'text_default',
      0                     => array(
        '#markup' => check_plain($db_url),
      ),
    );

    $content['chado_schema'] = array(
      '#theme'               => 'field',
      '#weight'              => -1,
      '#title'               => t('Chado schema'),
      '#access'              => TRUE,
      '#label_display'       => 'inline',
      '#view_mode'           => 'full',
      '#language'            => LANGUAGE_NONE,
      '#field_translatable' => FALSE,
      '#field_name'         => 'chado_schema',
      '#field_type'         => 'text',
      '#entity_type'        => TRIPAL_MC_CONNECTION_ENTITY_TYPE,
      '#bundle'             => TRIPAL_MC_CONNECTION_ENTITY_TYPE,
      '#items'              => array(array('value' => $entity->schemaname)),
      '#formatter'          => 'text_default',
      0                     => array(
        '#markup' => check_plain($entity->schemaname),
      ),
    );

    $content['description'] = array(
      '#theme'              => 'field',
      '#weight'             => 1,
      '#title'              => t('Description'),
      '#access'             => TRUE,
      '#label_display'      => 'above',
      '#view_mode'          => 'full',
      '#language'           => LANGUAGE_NONE,
      '#field_translatable' => TRUE,
      '#field_name'         => 'description',
      '#field_type'         => 'text',
      '#entity_type'        => TRIPAL_MC_CONNECTION_ENTITY_TYPE,
      '#bundle'             => TRIPAL_MC_CONNECTION_ENTITY_TYPE,
      '#items'              => array(array('value' => $entity->description)),
      '#formatter'          => 'text_default',
      0                     => array(
        '#markup' => check_plain($entity->description),
      ),
    );

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

  /**
   * Loads connections and decrypt passwords on the fly.
   */
  public function load($ids = FALSE, $conditions = array()) {
    $chado_connections = parent::load($ids, $conditions);
    foreach ($chado_connections as $chado_connection) {
      // Freshly loaded connection do not have 'password_decrypted' property
      // and therefor have a crypted password.
      if (!property_exists($chado_connection, 'password_decrypted')
          || !$chado_connection->password_decrypted) {
        $chado_connection->password = tripal_mc_decrypt_password($chado_connection->password);
        $chado_connection->password_decrypted = TRUE;
      }
    }
    return $chado_connections;
  }

  /**
   * Saves a connection data: saves crypted password.
   */
  public function save($chado_connection, DatabaseTransaction $transaction = NULL) {
    $return_value = NULL;
    // New connections do not have 'password_decrypted' property and if
    // existing ones do, check if password is crypted or not.
    if (!property_exists($chado_connection, 'password_decrypted')
        || $chado_connection->password_decrypted) {
      // We got a clear password.
      $clear_password = $chado_connection->password;
      $chado_connection->password = tripal_mc_encrypt_password($chado_connection->password);
      $return_value = parent::save($chado_connection, $transaction);
      $chado_connection->password = $clear_password;
    }
    else {
      $return_value = parent::save($chado_connection, $transaction);
    }
    // Update path alias.
    if ($return_value) {
      // Get connection URI.
      $chado_connection_uri = TRIPAL_MC_CONNECTION_ENTITY_TYPE . '/' . $chado_connection->mccid;
      // Get previous alias if one.
      $old_path = path_load(
        array('source' => $chado_connection_uri)
      );
      // And remove it.
      if ($old_path) {
        path_delete($old_path['pid']);
      }
      // Add new alias.
      $alias =
        TRIPAL_MC_CONNECTION_BASE_ALIAS
        . '/'
        . $chado_connection->machine_name;
      $path_alias = array(
        'source' => $chado_connection_uri,
        'alias' => $alias,
      );
      path_save($path_alias);
    }
    return $return_value;
  }

  /**
   * Removes connections and their associated data.
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    // Remove previous path aliases.
    foreach ($ids as $id) {
      $chado_connection_uri = TRIPAL_MC_CONNECTION_ENTITY_TYPE . '/' . $id;
      // Get previous alias if one.
      $old_path = path_load(
        array('source' => $chado_connection_uri)
      );
      // And remove it.
      if ($old_path) {
        path_delete($old_path['pid']);
      }
    }
    // Remove corresponding entries in sync tables.
    $sync_tables = tripal_mc_get_sync_tables();
    $num_deleted = 0;
    foreach ($sync_tables as $sync_table => $sync_enabled) {
      // We don't take in account the sync status as we remove data and
      // we don't want to have orphaned data.
      // We don't need to check if the ids are valid because if they don't
      // then we shouldn't have any associated entry anyway!
      // Security note: TRIPAL_MC_SYNC_TABLE_PREFIX and
      // TRIPAL_MC_CONNECTION_COLUMN are "constants" defined in
      // api/tripal_mc.const.inc file.
      $num_deleted += db_delete(TRIPAL_MC_SYNC_TABLE_PREFIX . $sync_table)
        ->condition(TRIPAL_MC_CONNECTION_COLUMN, $ids)
        ->execute();
    }
    return parent::delete($ids, $transaction);
  }

}
