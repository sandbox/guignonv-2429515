<?php

/**
 * @file
 * This file contains the functions used for administration of the module.
 */

/**
 * Returns the Chado Connection add form.
 *
 * Callback for hook_menu().
 *
 * @return array
 *   The form structure.
 *
 * @ingroup callbacks
 */
function tripal_mc_connection_admin_add_page() {
  // Get current database name to use it as default Chado database name.
  global $databases, $user;
  $default_database = $databases['default']['default']['database'];

  $chado_connection = entity_create(
    'tripal_mc_connection',
    array(
      'mccid'        => '',
      'title'        => 'Chado Connection',
      'machine_name' => '',
      'host'         => 'localhost',
      'port'         => '5432',
      'username'     => '',
      'password'     => '',
      'database'     => $default_database,
      'schemaname'   => 'chado',
      'description'  => '',
      'uid'          => $user->uid,
      'type'         => 'tripal_mc_connection',
    )
  );

  return drupal_get_form('tripal_mc_connection_form', $chado_connection);
}

/**
 * Form constructor for the Chado Connection add/edit form.
 *
 * @param array $form
 *   A Drupal form structure.
 * @param array $form_state
 *   A Drupal form state structure.
 * @param object $chado_connection
 *   A Chado connection object (TripalMCConnection).
 *
 * @see tripal_mc_connection_form_submit()
 * @see tripal_mc_connection_form_submit_delete()
 *
 * @ingroup forms
 */
function tripal_mc_connection_form(array $form, array &$form_state, $chado_connection) {

  $form_state['chado_connection'] = $chado_connection;

  // This is necessary for field_default_form() not to yell at us.
  $form['#parents'] = array();

  // These variables are needed to build field forms.
  // Substitute with any entity type.
  $entity_type = 'tripal_mc_connection';
  // Substitute with the entity's bundle.
  $bundle_name = 'tripal_mc_connection';
  // Not really necessary but we keep the parameter names consistent with the
  // field_default_form() signature.
  $entity = $chado_connection;
  // Substitute as appropriate.
  $langcode = LANGUAGE_NONE;

  // $items is used to provide default values for the field. You can set it to
  // NULL or an empty array if desired.
  $items = field_get_items($entity_type, $entity, TRIPAL_MC_ASSOCIATED_USER_FIELD);

  // We need the field and field instance information to pass along to
  // field_default_form().
  $field = field_info_field(TRIPAL_MC_ASSOCIATED_USER_FIELD);
  $instance = field_info_instance(
    $entity_type,
    TRIPAL_MC_ASSOCIATED_USER_FIELD,
    $bundle_name
  );

  // This is where the cool stuff happens.
  $field_form = field_default_form(
    $entity_type,
    $entity,
    $field,
    $instance,
    $langcode,
    $items,
    $form,
    $form_state
  );
  // Before description field.
  $field_form[TRIPAL_MC_ASSOCIATED_USER_FIELD]['#weight'] = 9;

  // Machine name stuff.
  $machine_name_pattern = '[^0-9a-zA-Z-]+';
  $machine_name_replacement = '-';

  $form = array(
    'title' => array(
      '#type'          => 'textfield',
      '#title'         => t('Name of the Chado connection'),
      '#description'   => t('This label is for administrative purpose and will be used to identify the following connection settings on administration interfaces.'),
      '#required'      => TRUE,
      '#default_value' => $chado_connection->title,
      '#field_suffix' => '<br/>',
    ),
    'machine_name' => array(
      '#type' => 'machine_name',
      '#default_value' => $chado_connection->machine_name,
      '#maxlength' => 24,
      '#machine_name' => array(
        'exists' => 'tripal_mc_connection_name_exists',
        'source' => array('title'),
        'label' => t('Connection machine name (unique identifier)'),
        'replace_pattern' => $machine_name_pattern,
        'replace' => $machine_name_replacement,
      ),
      '#required'      => TRUE,
    ),
    // Not supported at the moment.
    'host' => array(
      '#type'          => 'textfield',
      '#title'         => t('Host'),
      '#description'   => t('Host name like "www.host.com" or IP like "123.45.67.89". Default: "localhost" (or 127.0.0.1).<br/>Custom value is currently not supported for this field.'),
      '#required'      => FALSE,
      '#default_value' => $chado_connection->host,
      '#disabled'      => TRUE,
    ),
    // Not supported at the moment.
    'port' => array(
      '#type'          => 'textfield',
      '#title'         => t('Port'),
      '#description'   => t('PostgreSQL port used (default: 5432).<br/>Custom value is currently not supported for this field.'),
      '#required'      => FALSE,
      '#default_value' => $chado_connection->port,
      '#disabled'      => TRUE,
    ),
    'username' => array(
      '#type'          => 'textfield',
      '#title'         => t('User login'),
      '#description'   => t('User login used to connect to the database.'),
      '#required'      => TRUE,
      '#default_value' => $chado_connection->username,
    ),
    'password' => array(
      '#type'          => 'password',
      '#title'         => t('Password'),
      '#required'      => TRUE,
      '#attributes'    => array('value' => $chado_connection->password),
    ),
    'password_confirm' => array(
      '#type'          => 'password',
      '#title'         => t('Password confirmation'),
      '#required'      => TRUE,
      '#attributes'    => array('value' => $chado_connection->password),
    ),
    // Not supported at the moment.
    'database' => array(
      '#type'          => 'textfield',
      '#title'         => t('Database'),
      '#description'   => t('Name of the database.<br/>Custom value is currently not supported for this field.'),
      '#required'      => TRUE,
      '#default_value' => $chado_connection->database,
      '#disabled'      => TRUE,
    ),
    'schemaname' => array(
      '#type'          => 'textfield',
      '#title'         => t('Chado schema'),
      '#description'   => t('Database schema in which Chado is installed (default: "chado"). This can be set to "public" if Chado is installed on a separate database (ie. not the Drupal/Tripal one).'),
      '#required'      => FALSE,
      '#default_value' => $chado_connection->schemaname,
    ),
    'description' => array(
      '#type'          => 'textarea',
      '#title'         => t('Connection description'),
      '#description'   => t('This field is for administration purpose only.'),
      '#required'      => FALSE,
      '#default_value' => $chado_connection->description,
      // Last field after user association field.
      '#weight'        => 10,
    ),
    'uid' => array(
      '#type' => 'value',
      '#value' => $chado_connection->uid,
    ),
  );

  // Adding it to the form.
  $form += $field_form;

  field_attach_form(
    'tripal_mc_connection_form',
    $chado_connection,
    $form,
    $form_state
  );

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type'   => 'submit',
    '#value'  => t('Save'),
    '#submit' => $submit + array('tripal_mc_connection_form_submit'),
  );

  // Show Delete button if we edit Chado Connection.
  $chado_connection_id = entity_id('tripal_mc_connection', $chado_connection);
  if (!empty($chado_connection_id)
      && tripal_mc_connection_access('edit', $chado_connection)) {
    $form['actions']['delete'] = array(
      '#type'   => 'submit',
      '#value'  => t('Delete'),
      '#submit' => array('tripal_mc_connection_form_submit_delete'),
    );
  }

  return $form;
}

/**
 * Chado Connection submit handler.
 */
function tripal_mc_connection_form_submit($form, &$form_state) {
  $chado_connection = $form_state['chado_connection'];
  entity_form_submit_build_entity('tripal_mc_connection', $chado_connection, $form, $form_state);

  tripal_mc_connection_save($chado_connection);

  $chado_connection_uri = entity_uri(
    TRIPAL_MC_CONNECTION_ENTITY_TYPE,
    $chado_connection
  );

  $form_state['redirect'] = $chado_connection_uri['path'];

  drupal_set_message(
    t(
      'Chado connection %title saved.',
      array('%title' => entity_label('tripal_mc_connection', $chado_connection))
    )
  );
}

/**
 * Sends to the Chado connection delete form.
 */
function tripal_mc_connection_form_submit_delete($form, &$form_state) {
  $chado_connection = $form_state['chado_connection'];
  $chado_connection_uri = entity_uri(
    TRIPAL_MC_CONNECTION_ENTITY_TYPE,
    $chado_connection
  );
  $form_state['redirect'] = $chado_connection_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function tripal_mc_connection_delete_form($form, &$form_state, $chado_connection) {
  $form_state['chado_connection'] = $chado_connection;
  // Always provide entity id in the same form key as in the entity edit form.
  $chado_connection_uri = entity_uri(
    TRIPAL_MC_CONNECTION_ENTITY_TYPE,
    $chado_connection
  );
  return confirm_form(
    $form,
    t(
      'Are you sure you want to delete Chado connection %title?',
      array(
        '%title' => entity_label(
          TRIPAL_MC_CONNECTION_ENTITY_TYPE,
          $chado_connection
        ),
      )
    ),
    $chado_connection_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function tripal_mc_connection_delete_form_submit($form, &$form_state) {
  $chado_connection = $form_state['chado_connection'];
  tripal_mc_connection_delete($chado_connection);

  drupal_set_message(
    t(
      'Chado connection %title deleted.',
      array(
        '%title' => entity_label(
          TRIPAL_MC_CONNECTION_ENTITY_TYPE,
          $chado_connection
        ),
      )
    )
  );

  $form_state['redirect'] = '<front>';
}

/**
 * Access callback for Multi-Chado Connection.
 */
function tripal_mc_connection_access($op, $tripal_mc, $account = NULL, $entity_type = NULL) {
  global $user;

  if (!isset($account)) {
    $account = $user;
  }

  switch ($op) {
    case 'create':
      return user_access('administer multi chado', $account)
        || user_access('create tripal_mc_connection entities', $account);

    case 'view':
      return user_access('administer multi chado', $account)
        || user_access('view tripal_mc_connection entities', $account);

    case 'edit':
      return user_access('administer multi chado')
        || user_access('edit any tripal_mc_connection entities')
        || (user_access('edit own tripal_mc_connection entities')
          && ($tripal_mc->uid == $account->uid));
  }
}

/**
 * Callback to check if a connection machine name is already in use.
 */
function tripal_mc_connection_name_exists($value) {
  $name_exists = db_query_range(
    'SELECT 1 FROM {' . TRIPAL_MC_CONNECTION_TABLE . '} WHERE machine_name = :name',
    0,
    1,
    array(':name' => $value)
  )->fetchField();

  return $name_exists;
}
