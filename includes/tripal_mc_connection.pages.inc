<?php

/**
 * @file
 * Chado Connection page functions.
 */

/**
 * Returns a Chado Connection detail page.
 *
 * Callback for hook_menu().
 *
 * @return array
 *   The form structure.
 *
 * @ingroup callbacks
 */
function tripal_mc_connection_view($tmcc, $view_mode = 'full', $langcode = NULL) {
  drupal_set_title(entity_label(TRIPAL_MC_CONNECTION_ENTITY_TYPE, $tmcc));
  return entity_view(TRIPAL_MC_CONNECTION_ENTITY_TYPE, array(entity_id(TRIPAL_MC_CONNECTION_ENTITY_TYPE, $tmcc) => $tmcc), 'full');
}
