<?php

/**
 * @file
 * TripalMCConnection class definition.
 */

/**
 * Tripal Multi-Chado Connection entity.
 */
class TripalMCConnection extends Entity {

  /**
   * Returns Chado connection label.
   */
  protected function defaultLabel() {
    return $this->title;
  }

  /**
   * Returns Chado connection URI.
   */
  protected function defaultUri() {
    return array('path' => 'tripal_mc_connection/' . $this->identifier());
  }

}
