<?php

/**
 * @file
 * Handles Trupal Multi-Chado rules stuff.
 */

/**
 * Implements hook_rules_data_type_info().
 *
 * @ingroup tripal_mc
 */
function tripal_mc_rules_data_type_info() {
  return array(
    'tripal_multichado' => array(
      'label' => t('Chado connection'),
      'class' => 'TripalMCConnection',
      'savable' => TRUE,
      'identifiable' => TRUE,
      'uses_input_form' => FALSE,
      'module' => 'Tripal Multi-Chado',
    ),
  );
}

/**
 * Implements hook_rules_event_info().
 *
 * @ingroup tripal_mc
 */
function tripal_mc_rules_event_info() {
  return array(
    'tripal_mc_active_connection' => array(
      'module' => 'tripal_mc',
      'group' => t('Multi-Chado connection'),
      'label' => t('A new Tripal Multi-Chado connection is used'),
      'variables' => array(
        'active_connection' => array(
          'type' => 'tripal_mc_connection',
          'label' => t('Activated connection.'),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_condition_info().
 */
function tripal_mc_rules_condition_info() {
  return array(
    'tripal_mc_compare_current_connection_title' => array(
      'label' => t('Compare current connection title with a given one'),
      'arguments' => array(
        'connection_title' => array(
          'type' => 'text',
          'label' => t('The connection title to compare with.'),
        ),
      ),
      'group' => t('Tripal Multi-Chado'),
      'base' => 'tripal_mc_rules_condition_compare_connection_titles',
    ),
    'tripal_mc_compare_current_connection_mccid' => array(
      'label' => t('Compare current connection mccid with a given one'),
      'arguments' => array(
        'connection_mccid' => array(
          'type' => 'integer',
          'label' => t('The connection mccid to compare with.'),
        ),
      ),
      'group' => t('Tripal Multi-Chado'),
      'base' => 'tripal_mc_rules_condition_compare_connection_mccid',
    ),
  );
}

/**
 * Compare current Chado connection title with a given string.
 *
 * @see tripal_mc_rules_condition_info()
 *
 * @ingroup tripal_mc
 */
function tripal_mc_rules_condition_compare_connection_titles($args, $element) {
  $connection = tripal_mc_get_user_connection();
  $same_title = FALSE;
  if ($connection) {
    $same_title = ($connection->title == trim($element['connection_title']));
  }
  return $same_title;
}

/**
 * Compare current Chado connection mccid with a given mccid (integer).
 *
 * @see tripal_mc_rules_condition_info()
 *
 * @ingroup tripal_mc
 */
function tripal_mc_rules_condition_compare_connection_mccid($args, $element) {
  $connection = tripal_mc_get_user_connection();
  $same_mccid = FALSE;
  if ($connection) {
    $same_mccid = ($connection->mccid == trim($element['connection_mccid']));
  }
  return $same_mccid;
}

/**
 * Implements hook_rules_action_info().
 *
 * @ingroup tripal_mc
 */
function tripal_mc_rules_action_info() {
  return array(
    'tripal_mc_active_connection' => array(
      'label' => t('Load current (active) Chado connection'),
      'group' => t('Tripal Multi-Chado'),
      'provides' => array(
        'connection' => array(
          'label' => 'Current Chado connection',
          'type' => 'tripal_mc_connection',
          'save' => FALSE,
        ),
      ),
      'base' => 'tripal_mc_rules_action_load_active_connection',
    ),
    'tripal_mc_user_connections' => array(
      'label' => t('Load current user available Chado connections'),
      'group' => t('Tripal Multi-Chado'),
      'provides' => array(
        'connections' => array(
          'label' => 'Current user Chado connections',
          'type' => 'list<tripal_mc_connection>',
          'save' => FALSE,
        ),
      ),
      'base' => 'tripal_mc_rules_action_load_user_connections',
    ),
  );
}

/**
 * Loads and return current Chado connection.
 *
 * @see tripal_mc_rules_action_info()
 *
 * @ingroup tripal_mc
 */
function tripal_mc_rules_action_load_active_connection($args, $element) {
  $connection = tripal_mc_get_user_connection();
  return array('connection' => $connection);
}

/**
 * Loads and return current user Chado connections.
 *
 * @see tripal_mc_rules_action_info()
 *
 * @ingroup tripal_mc
 */
function tripal_mc_rules_action_load_user_connections($args, $element) {
  $connections = tripal_mc_get_user_available_connections();
  return array('connections' => $connections);
}
